# laboratorio II (2023/24)
Questo repository contiene le istruzioni per avviare un container docker con strumenti base per la programmazione in C e connettersi ad esso per le esercitazioni in classe.

## Installazione :gear:
Seguire passo-passo le istruzioni sottostanti, copia-incollando i comandi nel terminale.

### Installare docker :whale:
Di seguito le istruzioni per l'installazione di docker su macchine Linux e Windows. Per «terminale» s'intende:
- Powershell per gli utenti Windows. È possibile avviare Powershell anche dal menù a tendina collegato alla scorciatoia da tastiera <kbd>Windows</kbd> + <kbd>x</kbd>
- Bash (o altre shell di linea di comando) in macchine linux. È possibile aprire il terminale anche tramite la scorciatoia <kbd>ctrl</kbd> + <kbd>alt</kbd> + <kbd>t</kbd>

:warning: *Nota bene*: Gli utenti linux devono aggiungere `sudo` prima del comando `docker` in tutte le istruzioni che seguono (per esempio: `sudo docker build ...`, `sudo docker run ...`).

#### Passaggio 1 (Windows)
Installare Docker Desktop per Windows: https://docs.docker.com/desktop/install/windows-install/

### Passaggio 1 (Mac)
Installare Docker Desktop per Mac: https://docs.docker.com/desktop/install/mac-install/

#### Passaggio 1 (Linux)
Installare Docker Desktop per Linux: https://docs.docker.com/desktop/install/linux-install/

*In alternativa*, si può installare docker tramite `apt-get` come segue.
Aggiungere la chiave ufficiale GPG di docker:
```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```

Aggiungere il repository alle sorgenti apt:
```
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

#### Passaggio 2: Verificare l'installazione
:bulb: (*Opzionale*) Per controllare se docker è stato installato correttamente, si può eseguire `docker --version` che stampa su schermo la versione di docker; lanciando il comando `docker run hello-world` dovrebbe comparire un breve messaggio che inizia con «Hello from Docker!».

#### Passaggio 3: Creare un container docker 
Scaricare/copiare il file di testo `Dockerfile` di questo repository sul proprio PC, all'interno di una nuova cartella del proprio filesystem.

:warning: *Nota bene*: usare esattamente lo stesso nome (`Dockerfile`), con 'D' iniziale maiuscola e senz'alcun'estensione come `.txt` e simili.

:bulb: *Suggerimento per utenti Windows 11*: per visualizzare le estensioni dei file, accedere a `Impostazioni > Privacy e sicurezza > Esplora file` e attivare «Mostra estensioni file».

Aprire il terminale dei comandi, e posizionarsi tramite il comando `cd` nella cartella che contiene il `Dockerfile`. Eseguire `docker build -t lab2 .` per creare una nuova immagine docker chiamata «lab2».

:warning: *Nota bene*: il comando `build` termina con un punto fermo (`.`), che sta a indicare di costruire l'immagine docker nella cartella corrente.
:bulb: *Suggerimento*: Se, durante l'esecuzione di `docker build`, si riscontrano problemi nello scaricare l'immagine docker di ubuntu, provare a eseguire `docker pull ubuntu:20.04`

Avviare un container istanza di «lab2» tramite il comando `docker run -d -p 2222:22 lab2`

:bulb: *Suggerimento*: Se la porta `2222` dovesse risultare già occupata da altre applicazioni, usare invece sia qui sia nel seguito la porta `2223`, oppure la `2224`, la `2225`, ...

### Installare e configurare Visual Studio Code :writing_hand:
Per l'installazione, seguire le istruzioni su https://code.visualstudio.com/
Al termine dell'installazione, avviare Visual Studio Code.
Accedere al gestore delle estensioni facendo clic sulla barra laterale di sinistra (la scorciatoia da tastiera è <kbd>ctrl</kbd> + <kbd>maiusc</kbd> + <kbd>x</kbd>).
Installare l'estensione «Remote - SSH».
Al termine dell'installazione, nella barra di ricerca posizionata in alto scrivere `>` (la scorciatoia da tastiera è <kbd>F1</kbd>) e poi selezionare dal menù a tendina l'opzione «Remote-SSH: Connect to Host...». Selezionare quindi «+ Add New SSH Host» e inserire `ssh -p 2222 root@localhost`; la password da inserire è `lab2`. Seguire la procedura guidata e, alla richiesta «Select SSh configuration file to update», selezionare una delle opzioni proposte (per esempio, la prima).

:bulb: Suggerimento per utenti Windows: Se si riscontrano problemi di connessione, potrebb'essere utile accedere al percorso `C:\Users\nomeutente\.ssh` e aggiungere al file `config` il seguente testo:
```
Host lab2
  HostName 127.0.0.1
  Port 2222
  User root
```
A questo punto, riprovate ad accedere a «Remote-SSH: Connect to Host...», selezionando «lab2» dal menù a tendina.

Siete ora connessi al container di docker.

:bulb: (*Opzionale*) Nel percorso di default troverete un programma di esempio, chiamato `esempio.c`. Potete:
- Visualizzarlo e modificarlo scrivendo sul terminale di Visual Studio Code il comando `code esempio.c` (in alternativa, anche da `File > Open File...` oppure tramite scorciatoia con <kbd>ctrl</kbd> + <kbd>o</kbd>). Nota: il terminale di Visual Studio Code è posizionato nella parte inferiore della finestra di Visual Studio Code.
- Compilare il programma tramite il comando `gcc esempio.c -o esempio`
- Eseguire il codice compilato lanciando tramite il comando `./esempio`. Il programma d'esempio stampa a schermo la frase «Ciao mondo!».

### Domande frequenti
* **Come gestire i container in esecuzione?**
In ogni momento è possibile visualizzare la lista di container in esecuzione tramite il comando `docker ps`, che mostra una lista di elementi formattata come in questo esempio: `6481d88d750a   lab2      "/usr/sbin/sshd -D"   34 minutes ago   Up 34 minutes   0.0.0.0:2222->22/tcp   xenodochial_gould` (in questo caso è in esecuzione un container di nome `xenodochial_gould`, istanza dell'immagine `lab2`).
È possibile arrestare l'esecuzione del container e eseguirne la rimozione tramite i comandi
```bash
docker stop xenodochial_gould
docker rm xenodochial_gould
```
* **Come spostare file all'interno del container?**
Per spostare file da e verso il container, si può, ad esempio, usare FileZilla: https://filezilla-project.org/
Terminata l'installazione, avviare FileZilla e, per eseguire una connessione al container, compilare i campi come segue e cliccare su «Connessione rapida».
    - Host: sftp://127.0.0.1
    - Nome utente: root
    - Password: lab2
    - Porta: 2222