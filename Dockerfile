#docker build -t lab2 .
#docker run -d -p 2222:22 lab2
#ssh -p 2222 root@localhost

# Utilizza un'immagine di base con sistema operativo Ubuntu 20.04
FROM ubuntu:20.04

# Aggiorna i repository e installa gli aggiornamenti del sistema
RUN apt update && apt upgrade -y

# Installa i pacchetti necessari
RUN apt install -y cmake
RUN apt install -y gcc
RUN apt install -y gcc-doc
RUN apt install -y gdb
RUN apt install -y git
RUN apt install -y make
RUN apt install -y make-doc
RUN apt install -y manpages-posix
RUN apt install -y ssh
RUN apt install -y valgrind

# Crea codice d'esempio
RUN echo '#include <stdio.h>\n\nint main() {\n    printf("Ciao mondo!");\n    return 0;\n}' > /root/esempio.c

# Abilita il servizio SSH
RUN apt install -y openssh-server
RUN mkdir /var/run/sshd
RUN echo 'root:lab2' | chpasswd
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN echo 'PermitUserEnvironment yes' >> /etc/ssh/sshd_config
RUN service ssh restart

# Espone la porta 22 per SSH
EXPOSE 22

# Esegui il comando di default quando il container viene avviato
CMD ["/usr/sbin/sshd", "-D"]